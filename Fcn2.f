      SUBROUTINE Fcn(n,t,y,ydot,RPAR,IPAR)
      IMPLICIT NONE
	                                
C Global variable(s):
      INTEGER n, IPAR
      REAL*8 t, y, ydot, RPAR                                       
      DIMENSION y(*),ydot(n), RPAR(*), IPAR(*) 

C Common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
	                                           
C Local variable(s):
      INTEGER i, j, jr1, jr2, jp1, jp2, jp3, jp4, jp5, jrac1, jrac2
      REAL*8 term, rracc0, rrdes0, rracc1, rrdes1, rrdiff0, rrdiff1, 
     1   rda_min0, rda_min1, sterm0, sterm1, yy1, yy2, yy1g, yy2g
      
C Initialization of the arrayes:
      ydot = 0.0D0

C Construct ODE system "on the fly":
C Update surface reaction rates if modified rate approach is chosen:
      IF (mrat) THEN	         
        do i = 1, nr_surf
          j = ind_surf(i)
  	  rrdiff0 = Rdiff0(j)	  
	  rrdiff1 = Rdiff1(j)       
	  jrac1   = irac1(j)
  	  jrac2   = irac2(j)	
	  yy1g    = y(jrac1)/ddens
	  yy2g    = y(jrac2)/ddens	    
  	  jr1     = ir1(j)
	  jr2     = ir2(j)	  
          yy1     = y(jr1)/ddens
          yy2     = y(jr2)/ddens  	    
	  sterm0  = rrdiff0
	  sterm1  = rrdiff1

C Homogeneous reactions:        
          if (jr1.eq.jr2) then
 	    rracc0   = Racc(iar1(j))*yy1g
 	    rrdes0   = Rdes(idr1(j))
	    rda_min0 = DMAX1(rracc0,rrdes0)	        
            sterm0   = DMIN1(rrdiff0,rda_min0)
            sterm1   = 0.0d0
C Heterogeneous reactions:        
          else
	    rracc0   = Racc(iar1(j))*yy1g
	    rrdes0   = Rdes(idr1(j))
	    rda_min0 = DMAX1(rracc0,rrdes0)	        
            sterm0   = DMIN1(rrdiff0,rda_min0)	  
	    rracc1   = Racc(iar2(j))*yy2g     
	    rrdes1   = Rdes(idr2(j))
	    rda_min1 = DMAX1(rracc1,rrdes1)
	    sterm1   = DMIN1(rrdiff1,rda_min1)
          endif
          ak(j) = (sterm0+sterm1)*ak_dens(j)          
        enddo
      ENDIF                    

C 1) 1st-order kinetics, 1 product
      DO i = 1, nrc(1)
	jr1 = ir1(i)
	jp1 = ip1(i)
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
        ydot(jp1)=ydot(jp1)+term
      END DO
C 2) 1st-order kinetics, 2 products
      j = nrc(11)
      DO i = j+1, j+nrc(2)
	jr1 = ir1(i)
	jp1 = ip1(i)
	jp2 = ip2(i)	
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
      END DO      
C 3) 1st-order kinetics, 3 products
      j = nrc(12)
      DO i = j+1, j+nrc(3)
	jr1 = ir1(i)
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
      END DO            
C 4) 1st-order kinetics, 4 products
      j = nrc(13)
      DO i = j+1, j+nrc(4)
	jr1 = ir1(i)
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	jp4 = ip4(i)			
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
        ydot(jp4)=ydot(jp4)+term                        
      END DO                  
C 5) 1st-order kinetics, 5 products
      j = nrc(14)
      DO i = j+1, j+nrc(5)
	jr1 = ir1(i)
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	jp4 = ip4(i)			
	jp5 = ip5(i)				
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
        ydot(jp4)=ydot(jp4)+term                        
        ydot(jp5)=ydot(jp5)+term                                
      END DO                        
C 6) 2nd-order kinetics, 1 product
      j = nrc(15)
      DO i = j+1, j+nrc(6)
	jr1 = ir1(i)
	jr2 = ir2(i)
	jp1 = ip1(i)
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
        ydot(jp1)=ydot(jp1)+term
      END DO      
C 7) 2nd-order kinetics, 2 products
      j = nrc(16)
      DO i = j+1, j+nrc(7)
	jr1 = ir1(i)
	jr2 = ir2(i)
	jp1 = ip1(i)
	jp2 = ip2(i)	
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
      END DO            
C 8) 2nd-order kinetics, 3 products
      j = nrc(17)
      DO i = j+1, j+nrc(8)
	jr1 = ir1(i)
	jr2 = ir2(i)	
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
      END DO                  
C 9) 2nd-order kinetics, 4 products
      j = nrc(18)
      DO i = j+1, j+nrc(9)
	jr1 = ir1(i)
	jr2 = ir2(i)	
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	jp4 = ip4(i)			
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
        ydot(jp4)=ydot(jp4)+term                        
      END DO                        
C 10) 2nd-order kinetics, 5 products
      j = nrc(19)
      DO i = j+1, j+nrc(10)
	jr1 = ir1(i)
	jr2 = ir2(i)	
	jp1 = ip1(i)
	jp2 = ip2(i)	
	jp3 = ip3(i)		
	jp4 = ip4(i)			
	jp5 = ip5(i)				
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
        ydot(jp1)=ydot(jp1)+term
        ydot(jp2)=ydot(jp2)+term        
        ydot(jp3)=ydot(jp3)+term                
        ydot(jp4)=ydot(jp4)+term                        
        ydot(jp5)=ydot(jp5)+term                                
      END DO                              

C Exit:
      RETURN
      END
