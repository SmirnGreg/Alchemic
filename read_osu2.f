C..............................................................................
C
C This subroutine reads the names of chemical species from "specs".
C
C..............................................................................
C
C Version 1.2 (07/12/2004)
C
C..............................................................................
C
C Input parameter(s): 
C
C input   == a name of a file contains the names of the species,
C
C..............................................................................
C
C Output parameter(s):
C
C y(1:ny) == the names of species,
C
C..............................................................................
C
C Global parameter(s):
C
C nspec       == maximal amount of chemical species to be considered,
C
C..............................................................................
      subroutine reads(input)
      implicit none

C Global variable(s):
      character*80 input  ! the name of the input file,

C Local variable(s):
      integer i  ! counters for loops
      integer nlen !, first  ! a length of a string, the beginning of a string,

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
C
C Open input file(s):
C
      call len_tri2(input,80,nlen)
      open (unit=07,file=input(1:nlen),status='old',access='sequential')
      rewind 07
C
C Read 'y' from the file in the next loop:
C
      read (07,*) ns

      do i=1,ns
        read (07,*) s(i)
      enddo  

C Exit
      return
      end
C..............................................................................
C
C This subroutine reads a file with chemical reactions from OSU06 network.
C
C..............................................................................
C
C Version 1.4 (18.10.2006)
C
C..............................................................................
C
C Input parameter(s):
C
C input      == a name of a file with chemical reactions,
C
C..............................................................................
C
C Common block(s):
C
C..............................................................................
C
C BL1:
C
C ns           == amount of species,
C
C s(ns)        == a species set,
C
C nre          == amount of reactions in the input file,
C
C index(nre)    == indices of the corresponding chemical reactions,
C
C r1(nre)       == names of the first reactants,
C
C ir1(nre)      == array of indices of 'r1' species in extended species set 's',
C
C r2(nre)       == names of the second reactants,
C
C ir2(nre)      == array of indices of 'r2' species in extended species set 's',
C
C p1(nre)       == names of the first products, 
C
C ip1(nre)      == array of indices of 'p1' species in extended species set 's',
C
C p2(nre)       == names of the second products, 
C
C ip2(nre)      == array of indices of 'p2' species in extended species set 's',
C
C p3(nre)       == names of the third products, 
C
C ip3(nre)      == array of indices of 'p3' species in extended species set 's',
C
C p4(nre)       == names of the fourth products, 
C
C ip4(nre)      == array of indices of 'p4' species in extended species set 's',
C
C p5(nr)       == names of the fifth products, 
C
C ip5(nr)      == array of indices of 'p5' species in extended species set 's',
C
C alpha(nre)    == first components of the rate coeffs.,
C
C beta(nre)     == second components of the rate coeffs.,
C
C gamma(nre)    == third components of the rate coeffs.,
C
C rtype(nr)    == reaction type according to the OSU web-page,
C
C ak(nre)       == rate coefficients, computed from 'alpha', 'beta' and 'gamma',
C
C ak_dH2       == rate coefficient of H + H|grain -> H2 
C
C..............................................................................
C
C Used subroutines(s) (alphabetically): ispecia, len_tri2
C
C..............................................................................
      SUBROUTINE readr(input)
      IMPLICIT NONE
      
C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"      

C Global variable(s):
      CHARACTER*80 input

C Local variable(s):
      INTEGER i, nlen
      CHARACTER*12 pp
      character*9 reord_network
      dimension pp(4)

C Format(s):
 100  FORMAT (I6,1X,2(A12),12X,5(A12),3E9.2,1x,i2)
 101  FORMAT (I6,1X,2(A12),12X,5(A12),3(1pE9.2),1x,i2)

C Open input file:
      CALL len_tri2(input,80,nlen)
      OPEN (unit=10,file=input(1:nlen),status='old',
     &      access='sequential')

C..............................................................................
C Read chemical network data from input:
C..............................................................................
      READ (10, *) nre, reord_network
      print *, nre, reord_network
      DO i = 1, nre
        READ (10,100) index(i),r1(i),r2(i),p1(i),p2(i),p3(i),
     &    p4(i), p5(i), alpha(i), beta(i), gamma(i), rtype(i)
     
cd        write(55,101) index(i),r1(i),r2(i),p1(i),p2(i),p3(i),
cd     &    p4(i), p5(i), alpha(i), beta(i), gamma(i), rtype(i)

cd        write(*,101) index(i),r1(i),r2(i),p1(i),p2(i),p3(i),
cd     &    p4(i), p5(i), alpha(i), beta(i), gamma(i), rtype(i)     
     
C Remove null products:
        if (reord_network.ne.'Reordered') then
          pp(1:4) = [p2(i),p3(i),p4(i),p5(i)]        
          call remove_no_products(pp)
          p2(i) = pp(1)
          p3(i) = pp(2)
          p4(i) = pp(3)
          p5(i) = pp(4)
        endif

C Search positions of species:
        CALL ispecies(r1(i),ns,s,ir1(i))
        CALL ispecies(r2(i),ns,s,ir2(i))
        CALL ispecies(p1(i),ns,s,ip1(i))
        CALL ispecies(p2(i),ns,s,ip2(i))
        CALL ispecies(p3(i),ns,s,ip3(i))
        CALL ispecies(p4(i),ns,s,ip4(i))   
        CALL ispecies(p5(i),ns,s,ip5(i))

      ENDDO
      
C Reorder network: 
      call reorder_network_o1p1() 
      call reorder_network_o1p2()       
      call reorder_network_o1p3() 
      call reorder_network_o1p4()       
      call reorder_network_o1p5() 
      call reorder_network_o2p1()       
      call reorder_network_o2p2() 
      call reorder_network_o2p3()       
      call reorder_network_o2p4() 
      call reorder_network_o2p5()       
           
C Save original rates for updates of pd & pi UV rates:
      call aux_reacs()      
      
C Locate surface reactions and their total amount:
      call surface_reacs()      

cd      DO i = 1, nre
cd	write(55,101) index(i),r1(i),r2(i),p1(i),p2(i),p3(i),
cd     &    p4(i), p5(i), alpha(i), beta(i), gamma(i)
cd      ENDDO
cd      close(55)

cd      print *, nrc(1),nrc(2),nrc(3),nrc(4),nrc(5)
cd      print *, nrc(6),nrc(7),nrc(8),nrc(9),nrc(10)
            
C Close all files:
      CLOSE (10)

C Exit:
      RETURN
      END
C..............................................................................
C Reorder products such that null ones are in the end of the list:
C..............................................................................
      subroutine remove_no_products(p)
      implicit none

C Local variable(s):
      integer i, j
      character*12 p
      dimension p(4)
      
cd      print *,'p(old): ', p(1),p(2),p(3),p(4)
      
      do i = 1, 4
        if (p(i).eq.' ') then
          j = 4
          do while ((j.gt.i).and.(p(j).eq.' '))
            j = j - 1
          enddo
          call reorder_array_char(i,j,p)                                         
        endif          
      enddo
      
cd      print *,'p(new): ', p(1),p(2),p(3),p(4)
cd      pause
       
      RETURN
      END
C..............................................................................
C For given i and j reorder reaction data:
C..............................................................................
      subroutine reorder_reaction_data(i,j)
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i, j  
      
C Index of reaction:
      call reorder_array_int(j,i,index)
C 1st reactant:       
      call reorder_array_int(j,i,ir1)          
      call reorder_array_char(j,i,r1)         
C 2nd reactant:          
      call reorder_array_int(j,i,ir2)                     
      call reorder_array_char(j,i,r2)                                         
C 1st product:        
      call reorder_array_int(j,i,ip1)          
      call reorder_array_char(j,i,p1)          
C 2nd product:        
      call reorder_array_int(j,i,ip2)          
      call reorder_array_char(j,i,p2)                    
C 3rd product:        
      call reorder_array_int(j,i,ip3)          
      call reorder_array_char(j,i,p3)                              
C 4th product:        
      call reorder_array_int(j,i,ip4)          
      call reorder_array_char(j,i,p4)                                        
C 5th product:        
      call reorder_array_int(j,i,ip5)          
      call reorder_array_char(j,i,p5)                                                            
C Reaction data:
      call reorder_array_real(j,i,alpha)          
      call reorder_array_real(j,i,beta)          
      call reorder_array_real(j,i,gamma)                         
      call reorder_array_int(j,i,rtype)                               
      
      RETURN
      END
C..............................................................................
C
C Isolate all reactions such as:
C 1st-order kinetics, 1 product
C
C..............................................................................
      subroutine reorder_network_o1p1()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i, j      
      
C I) 1st-order kinetics, 1 product:
      nrc(1) = 0
      j = 0
      do i = j+1, nre
        if ((ir2(i).eq.0).and.(ip2(i).eq.0)) then
          nrc(1) = nrc(1) + 1
          j = j + 1
          call reorder_reaction_data(i,j) 
        endif
      enddo
      
      nrc(11) = nrc(1)
      
      RETURN
      END

C..............................................................................
C
C Isolate all reactions such as:
C 1st-order kinetics, 2 products
C
C..............................................................................
      subroutine reorder_network_o1p2()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i, j            
      
C II) 1st-order kinetics, 2 products:
      nrc(2) = 0
      j = nrc(11)
      do i = j+1, nre
        if ((ir2(i).eq.0).and.(ip3(i).eq.0)) then
          nrc(2) = nrc(2) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo      
      
      nrc(12) = nrc(11)+nrc(2)      
      
      RETURN
      END
      
C..............................................................................
C
C Isolate all reactions such as:
C 1st-order kinetics, 3 products
C
C..............................................................................
      subroutine reorder_network_o1p3()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 1st-order kinetics, 3 products:
      nrc(3) = 0
      j = nrc(12)
      do i = j+1, nre
        if ((ir2(i).eq.0).and.(ip4(i).eq.0)) then
          nrc(3) = nrc(3) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo            

      nrc(13) = nrc(12)+nrc(3)      
      
      RETURN
      END
C..............................................................................
C
C Isolate all reactions such as:
C 1st-order kinetics, 4 products
C
C..............................................................................
      subroutine reorder_network_o1p4()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 1st-order kinetics, 4 products:
      nrc(4) = 0
      j = nrc(13)
      do i = j+1, nre
        if ((ir2(i).eq.0).and.(ip5(i).eq.0)) then
          nrc(4) = nrc(4) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo                  

      nrc(14) = nrc(13)+nrc(4)      
      
      RETURN
      END
C..............................................................................
C
C Isolate all reactions such as:
C 1st-order kinetics, 5 products
C
C..............................................................................
      subroutine reorder_network_o1p5()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 1st-order kinetics, 5 products:
      nrc(5) = 0
      j = nrc(14)
      do i = j+1, nre
        if ((ir2(i).eq.0).and.(ip5(i).ne.0)) then
          nrc(5) = nrc(5) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo                  

      nrc(15) = nrc(14)+nrc(5)      
      
      RETURN
      END      
C..............................................................................
C
C Isolate all reactions such as:
C 2nd-order kinetics, 1 product
C
C..............................................................................
      subroutine reorder_network_o2p1()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j      
      
C I) 2nd-order kinetics, 1 product:
      nrc(6) = 0
      j = nrc(15)
      do i = j+1, nre
        if (ip2(i).eq.0) then
          nrc(6) = nrc(6) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo
      
      nrc(16) = nrc(15)+nrc(6)            
      
      RETURN
      END

C..............................................................................
C
C Isolate all reactions such as:
C 2nd-order kinetics, 2 products
C
C..............................................................................
      subroutine reorder_network_o2p2()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C II) 2nd-order kinetics, 2 products:
      nrc(7) = 0
      j = nrc(16)
      do i = j+1, nre
        if (ip3(i).eq.0) then
          nrc(7) = nrc(7) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo      
      
      nrc(17) = nrc(16)+nrc(7)                  
      
      RETURN
      END
      
C..............................................................................
C
C Isolate all reactions such as:
C 2nd-order kinetics, 3 products
C
C..............................................................................
      subroutine reorder_network_o2p3()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 2nd-order kinetics, 3 products:
      nrc(8) = 0
      j = nrc(17)
      do i = j+1, nre
        if (ip4(i).eq.0) then
          nrc(8) = nrc(8) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo
      
      nrc(18) = nrc(17)+nrc(8)                                    
      
      RETURN
      END
C..............................................................................
C
C Isolate all reactions such as:
C 2nd-order kinetics, 4 products
C
C..............................................................................
      subroutine reorder_network_o2p4()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 2nd-order kinetics, 4 products:
      nrc(9) = 0
      j = nrc(18)
      do i = j+1, nre
        if (ip5(i).eq.0) then
          nrc(9) = nrc(9) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
        endif
      enddo                  

      nrc(19) = nrc(18)+nrc(9)                                    
      
      RETURN
      END
C..............................................................................
C
C Isolate all reactions such as:
C 2nd-order kinetics, 5 products
C
C..............................................................................
      subroutine reorder_network_o2p5()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i,j            
      
C III) 2nd-order kinetics, 5 products:
      nrc(10) = 0
      j = nrc(19)
      do i = j+1, nre
          nrc(10) = nrc(10) + 1
          j = j + 1
          call reorder_reaction_data(i,j)
      enddo                  
      
      nrc(20) = nrc(19)+nrc(10)                                          
      
      RETURN
      END            
C..............................................................................
C
C This subroutine reorders elements of an integer array for 2 input indices
C..............................................................................
C
C Version 1.0 (17/06/2009)
C
C..............................................................................
C
C Input parameter(s): 
C
C i, j  -- permutation indices (current & desired),
C 
C iarr  -- integer array
C
C..............................................................................
C
C Output parameter(s): 
C
C iarr
C
C..............................................................................
      subroutine reorder_array_int(i,j,iarr)
      implicit none
      
C Global variables:
      integer i, j, iarr(*)

C Local variable(s):
      integer k
      
C reorder:
      k = iarr(i)      
      iarr(i) = iarr(j)
      iarr(j) = k
      
      RETURN
      END
C..............................................................................
C
C The same as reorder_array_int but for real*8 array
C..............................................................................
      subroutine reorder_array_real(i,j,arr)
      implicit none
      
C Global variables:
      integer i, j
      real*8 arr(*)

C Local variable(s):
      real*8 a
      
C reorder:
      a = arr(i)      
      arr(i) = arr(j)
      arr(j) = a
      
      RETURN
      END
C..............................................................................
C
C The same as reorder_array_int but for character*(*) array
C..............................................................................
      subroutine reorder_array_char(i,j,arr)
      implicit none
      
C Global variables:
      integer i, j
      character*12 arr(*)

C Local variable(s):
      character*12 a
      
C reorder:
      a = arr(i)      
      arr(i) = arr(j)
      arr(j) = a
      
      RETURN
      END

C..............................................................................
C
C This subroutine saves original reaction rates and makes a link between
C surface secies and their gas-phase parents
C..............................................................................
C
C Version 1.0 (17/06/2009)
C
C..............................................................................
C
C Input/Output parameter(s): 
C
C All data are transfered through common blocks in Fcn.h
C
C..............................................................................
      subroutine aux_reacs()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      integer i, nlen, nr1, np1
      CHARACTER*12 rr1, rr2, rr1s, rr2s      
      
C Save original reaction rates for the ISM UV pd and pi processes:      
      DO i = 1, nre         

        alpha_is(i) = alpha(i)
        beta_is(i) = beta(i)
        gamma_is(i) = gamma(i)         
         
C Identify photoionization reactions:         
        ion_reac(i) = 0
        if ((r2(i).eq.'PHOTON').and.((p2(i).eq.'ELECTR').or.
     .      (p3(i).eq.'ELECTR')) ) ion_reac(i) = 1
          
cd         write (44,*) i,r1(i),r2(i),p1(i),p2(i),p3(i),ion_reac(i)
         
C Find an index of a gas-phase molecule that corresponds to it's surface analogue:  
         rr1 = r1(i)
         rr2 = r2(i)
         
         if (rr2(1:1).eq.'g') then
         
           CALL len_tri2(rr1,12,nlen)
           rr1s = rr1(2:nlen)
           CALL ispecies(rr1s,ns,s,irac1(i))           
           
           CALL len_tri2(rr2,12,nlen)
           rr2s = rr2(2:nlen)         
           CALL ispecies(rr2s,ns,s,irac2(i))           
           
         endif  

      END DO     
      
      RETURN
      END
C..............................................................................
C
C This subroutine saves indices and total amount of surface reactions in the
C network
C..............................................................................
C
C Version 1.0 (17/06/2009)
C
C..............................................................................
C
C Input/Output parameter(s): 
C
C All data are transfered through common blocks in Fcn.h
C
C..............................................................................
      subroutine surface_reacs()
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"      
      
C Local variables:
      integer i
      character*12 rr2
      
      nr_surf = 0
      
      do i = 1, nre
        rr2 = r2(i)
        if (rr2(1:1).EQ.'g') then
          nr_surf = nr_surf+1
          ind_surf(nr_surf) = i
        endif
      enddo      
      
      mrat = (nr_surf.ne.0).and.(surf_method(1:4).eq.'MRAT')      
      
      RETURN
      END
      