C..............................................................................
C
C Global parameter(s):
C
C nreac       == maximal amount of chemical reactions to be read,
C
C nspec       == maximal amount of chemical species to be considered,
C
C nspec2      == maximal amount of initially present chemical species,
C
C ntime       == maximal amount of taken time steps,
C
C nss         == x nspec = amount of non-zero Jacobian elements
C 
C..............................................................................
C Global parameter(s):

      INTEGER nreac, nspec, nspec2, ntime, nss
      PARAMETER (nreac=100000,nspec=5000,nspec2=nspec,ntime=99,
     1  nss=nspec)
      