#!/bin/sh
code=disk_chemistry_OSU06ggs_UV_D
#ncpu=`sysctl -n hw.ncpu`  # Mac OS
ncpu=`cat /proc/cpuinfo | grep processor | wc -l`  # Linux
i=0
while [ $((i++)) -lt $ncpu ] ;
do
#  echo $i
  ./$code $ncpu $i >out_${i}& 
done 

# Extraction of abundances:
wait


